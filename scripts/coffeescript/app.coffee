console.log "fileground-demo"

angular.module('fileground', []).config () ->
	# nope

ExampleSingleCtrl = ($scope) ->

	ref = new FileGround.SingleUploadReference(SERV, {
		element: 'profile-image'
	})

	ref.on 'uploaded', (err, image) ->
		$scope.$apply () ->
			$scope.url = image.url

	$scope.upload = () ->
		ref.upload()
