var express = require('express'),
    coffeescript = require('connect-coffee-script'),
    jade_middleware = require('connect-jade-html'),
    jade = require('jade'),
    stylus = require('stylus'),
    nib = require('nib'),

    app = express();

app.configure(function() {
    app.set('port', process.env.PORT || 5000);

    app.set('views', __dirname + '/templates/jade');
    app.set('view engine', 'jade');

    app.use(express.logger('dev'));
    app.use(express.favicon());

    app.use('/scripts', coffeescript({
        src: __dirname + '/scripts/coffeescript',
        dest: __dirname + '/scripts/javascript',
        bare: true
    }));

    app.use('/templates', jade_middleware({
        src: __dirname + '/templates/jade',
        dest: __dirname + '/templates/html',
        pretty: true
    }));

    app.use('/styles', stylus.middleware({
        src: __dirname + '/styles/stylus',
        dest: __dirname + '/styles/css',
        compile: function (str, path, fn) {
            return stylus(str).set('filename', path).use(nib());
        }
    }));

    app.use('/scripts', express.static(__dirname + '/scripts/javascript'));
    app.use('/templates', express.static(__dirname + '/templates/html'));
    app.use('/styles', express.static(__dirname + '/styles/css'));
    app.use('/images', express.static(__dirname + '/images'));
    app.use('/test/images', express.static(__dirname + '/test/images'));

    app.use('/bower_components', express.static(__dirname + '/bower_components'));

    app.use(app.router);
});

app.get('/*', function(req, res, next) {
    if (req.headers.host.indexOf('localhost') !== -1) {
        serv = 'http://localhost:3000';
    } else {
        serv = 'http://imstuff-server.herokuapp.com'
    }

    res.render('index', {
        serv: serv
    });
});

app.listen(app.get('port'), function() {
    console.log("express listening on %d", app.get('port'));
});

